<?php

/**
 * @file
 * Provides an additional config form for theme settings.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_comment_FORM_ID_alter().
 */
function yg_interior_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {
  $form['visibility'] = [
    '#type' => 'vertical_tabs',
    '#title' => t('YG Interior Settings'),
    '#weight' => -999,
  ];
  $form['footer'] = [
    '#type' => 'details',
    '#title' => t('Footer Options'),
    '#weight' => -999,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
  $form['footer']['copyright'] = [
    '#type' => 'text_format',
    '#title' => t('Copyrights'),
    '#default_value' => theme_get_setting('copyright')['value'],
    '#description'   => t("Please enter the copyright content here."),
  ];

  $form['social'] = [
    '#type' => 'details',
    '#title' => t('Call Us & Social Links'),
    '#weight' => -999,
    '#group' => 'visibility',
    '#open' => FALSE,
  ];
  // Social links.
  $form['social']['call_us'] = [
    '#type' => 'details',
    '#title' => t('Call Us'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['social']['call_us']['call_us_title'] = [
    '#type' => 'textfield',
    '#title' => t('Call Us Title'),
    '#description' => t('Please enter your call us title'),
    '#default_value' => theme_get_setting('call_us_title'),
    '#required' => TRUE,
  ];
  $form['social']['call_us']['contact_number'] = [
    '#type' => 'textfield',
    '#title' => t('Contact Number'),
    '#description' => t('Please enter your contact number'),
    '#default_value' => theme_get_setting('contact_number'),
  ];
  $form['social']['call_us']['mail_id'] = [
    '#type' => 'textfield',
    '#title' => t('Email Id'),
    '#description' => t('Please enter your mail-id'),
    '#default_value' => theme_get_setting('mail_id'),
  ];
  $form['social']['social_links'] = [
    '#type' => 'details',
    '#title' => t('Social Links'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  ];
  $form['social']['social_links']['social_title'] = [
    '#type' => 'textfield',
    '#title' => t('Social Section Title'),
    '#description' => t('Please enter social section title'),
    '#default_value' => theme_get_setting('social_title'),
    '#required' => TRUE,
  ];
  $form['social']['social_links']['twitter_url'] = [
    '#type' => 'textfield',
    '#title' => t('Twitter'),
    '#description' => t('Please enter your twitter url'),
    '#default_value' => theme_get_setting('twitter_url'),
  ];
  $form['social']['social_links']['facebook_url'] = [
    '#type' => 'textfield',
    '#title' => t('Facebook'),
    '#description' => t('Please enter your facebook url'),
    '#default_value' => theme_get_setting('facebook_url'),
  ];
  $form['social']['social_links']['youtube_url'] = [
    '#type' => 'textfield',
    '#title' => t('Youtube'),
    '#description' => t('Please enter your youtube url'),
    '#default_value' => theme_get_setting('youtube_url'),
  ];
  $form['social']['social_links']['instagram_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram'),
    '#description' => t('Please enter your instagram url'),
    '#default_value' => theme_get_setting('instagram_url'),
  ];
  $form['social']['social_links']['linkedin_url'] = [
    '#type' => 'textfield',
    '#title' => t('Instagram'),
    '#description' => t('Please enter your linkedin url'),
    '#default_value' => theme_get_setting('linkedin_url'),
  ];

}
