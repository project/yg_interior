(function ($, Drupal) {
  Drupal.behaviors.myModuleBehavior = {
    attach: function (context, settings) {
      new WOW().init(); 
      
      $(document).ready(function() {
        $('#our-clients-carousel').owlCarousel({
          loop: true,
          margin: 10,
          responsiveClass: true,
          responsive: {
            0: {
              items: 1,
              nav: false
            },
            600: {
              items: 3,
              nav: false
            },
            1000: {
              items: 6,
              nav: false,
              loop: false,
              margin: 20
            }
          }
        })
      });

      $(document).ready(function(){
          $("#testimonial-slider").owlCarousel({
              responsiveClass: true,
          responsive: {
            0: {
              items: 1,
              nav: true
            },
            600: {
              items: 1,
              nav: true
            },
            1000: {
              items: 2,
              nav: true,
              loop: false,
              margin: 20
            }
          },

          });
          $(".next").click(function(){
          owl.trigger('owl.next');
        })
        $(".prev").click(function(){
          owl.trigger('owl.prev');
        })
      });

      $(document).ready(function(){
          $("#other-products-carousel").owlCarousel({
              responsiveClass: true,
          responsive: {
            0: {
              items: 1,
              nav: true
            },
            600: {
              items: 1,
              nav: true
            },
            1000: {
              items: 3,
              nav: true,
              loop: false,
              margin: 20
            }
          },

          });
          $(".next").click(function(){
          owl.trigger('owl.next');
        })
        $(".prev").click(function(){
          owl.trigger('owl.prev');
        })

          $(once('toggleBehavior','#toggle')).click(function() {
          $(this).toggleClass('active');
         $('#overlay').toggleClass('open');
        });
      });
      var $container = $('.product-section');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
      $('.productFilter a').click(function(){
          $('.productFilter .current').removeClass('current');
          $(this).addClass('current');
   
          var selector = $(this).attr('data-filter');
          $container.isotope({
              filter: selector,
              animationOptions: {
                  duration: 750,
                  easing: 'linear',
                  queue: false
              }
           });
           return false;
      });

      $('.region').removeClass('row');
    }
  };
})(jQuery, Drupal);    